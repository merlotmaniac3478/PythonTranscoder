#!/usr/bin/python

import os
from pathlib import Path, PurePath
import argparse
import subprocess

"""
Transcoding profiles for common devices
"""

PROFILES = \
    {\
        "PS4_copy_subs" : {\
            "video_codec" : ["-c:v", "libx264", "-crf", "18", "-vf", "format=yuv420p"],\
            "audio_codec" : ["-c:a", "ac3", "-b:a", "640k"],\
            "subtitle_opts" : ["-c:s", "copy"],\
            "suffix" : ".mkv"\
        },\
        "PS4_add_subs" : {\
            "video_codec" : ["-c:v", "libx264", "-crf", "18", "-vf", "format=yuv420p"],\
            "audio_codec" : ["-c:a", "ac3", "-b:a", "640k"],\
            "subtitle_opts" : ["-c:s", "srt"],\
            "suffix" : ".mkv"\
        },\
        "PS4" : {\
            "video_codec" : ["-c:v", "libx264", "-crf", "18", "-vf", "format=yuv420p"],\
            "audio_codec" : ["-c:a", "ac3", "-b:a", "640k"],\
            "subtitle_opts" : [],\
            "suffix" : ".mkv"\
        },\
        "x264_copy_subs" : {\
            "video_codec" : ["-c:v", "libx264", "-crf", "18", "-vf", "format=yuv420p"],\
            "audio_codec" : ["-c:a", "copy"],\
            "subtitle_opts" : ["-c:s", "copy"],\
            "suffix" : ".mkv"\
        },\
        "x264" : {\
            "video_codec" : ["-c:v", "libx264", "-crf", "18", "-vf", "format=yuv420p"],\
            "audio_codec" : ["-c:a", "copy"],\
            "subtitle_opts" : [],\
            "suffix" : ".mkv"\
        },\
        "ac3_copy_subs" : {\
            "video_codec" : ["-c:v", "copy"],\
            "audio_codec" : ["-c:a", "ac3", "-b:a", "640k"],\
            "subtitle_opts" : ["-c:s", "copy"],\
            "suffix" : ".mkv"\
        },\
        "ac3" : {\
            "video_codec" : ["-c:v", "copy"],\
            "audio_codec" : ["-c:a", "ac3", "-b:a", "640k"],\
            "subtitle_opts" : [],\
            "suffix" : ".mkv"\
        },\
        "subtitles" : {\
            "video_codec" : ["-c:v", "copy"],\
            "audio_codec" : ["-c:a", "copy"],\
            "subtitle_opts" : ["-c:s", "srt"],\
            "suffix" : ".mkv"\
        }\
    }

VERBOSE = False

def is_valid_file(file):
    ret = subprocess.run(["ffprobe", file], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    return ret.returncode == 0


def get_files(in_path, out_path, subtitle_dir, subtitle_file, profile):
    if not os.path.exists(in_path):
        raise Exception(f'{in_path} not found')

    print('Generating file list')
    files = []
    if os.path.isfile(in_path):
        out_file = get_output_file_name(in_path, out_path, in_path, profile)
        sub_file = None
        if None is not subtitle_dir:
            print(f'Getting subtitle file for input file {in_path}')
            sub_file = get_subtitle_file(subtitle_dir, subtitle_file, in_path)
        files.append((in_path, out_file, sub_file))
    elif os.path.isdir(in_path):
        for root, dirs, in_files in os.walk(in_path):
            for file in in_files:
                if VERBOSE:
                    print(f'Checking if {file} is a valid input file')
                in_file = os.path.join(root, file)
                if not in_file.endswith('.nfo') and not in_file.endswith('.srt') and is_valid_file(in_file):
                    if VERBOSE:
                        print(f'Getting matching output file for {in_file}')
                    out_file = get_output_file_name(in_path, out_path, in_file, profile)
                    sub_file = None
                    if None is not subtitle_dir:
                        if VERBOSE:
                            print(f'Getting subtitle file for input file {in_file}')
                        sub_file = get_subtitle_file(subtitle_dir, subtitle_file, in_file)
                    files.append((in_file, out_file, sub_file))
                    if VERBOSE:
                        print(f'Adding input file: {in_file}, output file: {out_file}, and subtitle file: {sub_file} for processing')
    return files


def get_output_file_name(in_path, out_path, file, profile):
    out_file = Path(file).stem + profile['suffix']
    if out_path is not None:
        if os.path.isdir(in_path):
            common_path = os.path.dirname(file).replace(in_path, '')
            if common_path.startswith('/'):
                common_path = common_path[1::]
            new_path = os.path.join(out_path, common_path)
            out_file = os.path.join(new_path, out_file)
        else:
            out_file = os.path.join(out_path, out_file)
    else:
        out_file = os.path.join(os.path.dirname(file), out_file)
    return out_file


def get_subtitle_file(subtitle_dir, subtitle_file, in_file):
    if None is not subtitle_file:
        media_name = Path(in_file).stem
        if VERBOSE:
            print(f'Attempting to match subtitle file parent directory to ${media_name}')
    if not os.path.isdir(subtitle_dir):
        raise Exception(f'{subtitle_dir} is not a directory')
    for root, dirs, files in os.walk(subtitle_dir):
        for file in files:
            if (None is not subtitle_file and file == os.path.basename(subtitle_file) and media_name == PurePath(root).name) or file == os.path.basename(in_file):
                if VERBOSE:
                    print(f'Found matching subtitle file {os.path.join(root, file)}')
                return os.path.join(root, file)
    raise Exception(f'No subtitle found in {subtitle_dir}: subtitle file: {subtitle_file} input file: {in_file}')


def transcode_videos(files, subtitle_language, profile, dry_run, has_subtitles):
    failures = []
    for in_file, out_file, sub_file in files:
        if VERBOSE:
            print(f'Generating args for file group ({in_file}, {out_file}, {sub_file})')
        if has_subtitles:
            args = ["ffmpeg", "-i", in_file, "-i", sub_file, "-map", "0", "-map", "1"]
            if None is not subtitle_language:
                args.extend(["-metadata:s:s:0", f'language={subtitle_language}'])
        else:
            args = ["ffmpeg", "-i", in_file, "-map", "0"]
        args.extend(profile['video_codec'])
        args.extend(profile['audio_codec'])
        args.extend(profile['subtitle_opts'])
        args.append(out_file)

        if dry_run:
            command = ""
            for arg in args:
                command += arg + ' '
            print(command)
        else:
            os.makedirs(os.path.dirname(out_file), exist_ok=True)
            ret = subprocess.run(args)
            if 0 != ret.returncode:
                print(f'Failed to transcode {in_file}: {ret.args}');
                failures.append((in_file, ret.args))
    for failure in failures:
        print(f'Failed to transcode file {failure[0]} with args {failure[1]}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script for transcoding videos")
    parser.add_argument("input", help="Input path or file to transcode. If a directory is given all valid files found in that directory and its sub directories will be transcoded")
    parser.add_argument("-p", "--profile", help="Specify which encoding profile to use. By default PS4 profile is used", default='PS4')
    parser.add_argument("-o", "--output_dir", help="Specify where the output file should be created. If nothing is specified the, the output file will be created in the original file's directory")
    parser.add_argument("-d", "--dry_run", help="Do not actually do anything just display what would be run", action="store_true")
    parser.add_argument("-s", "--subtitle_dir", help="Specify where we should look for subtitle streams to add. Will also search subdirectories. Subtitles should be srt format. " \
                                                    " If subtitle name does not match the directory name containing the subtitle file should match the media name if attempting to transcode more than one item")
    parser.add_argument("-l", "--subtitle_lang", help="Language of subtitle")
    parser.add_argument("-f", "--subtitle_file", help="Filename of the subtitle file if it will not match input file name")
    parser.add_argument("-v", "--verbose", help="Display additional information when running", action="store_true", default=False)

    args = parser.parse_args()

    VERBOSE=args.verbose

    profile = PROFILES[args.profile]

    if None is not args.subtitle_dir:
        has_subtitles = True

    if VERBOSE:
        print(f'Using transcoding profile {args.profile}')
        print(f'Got subtitle info: directory: {args.subtitle_dir}, language: {args.subtitle_lang}, file: {args.subtitle_file}')
        if args.dry_run:
            print("Performing dry run")

    files = get_files(os.path.abspath(args.input), args.output_dir, args.subtitle_dir, args.subtitle_file, profile)
    transcode_videos(files, args.subtitle_lang, profile, args.dry_run, has_subtitles)

